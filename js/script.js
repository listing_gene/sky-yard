/* $(function () {
    // ナビメニュー開閉
    $(function () {
        $('#nav-toggle').on('click', function () {
            $(this).toggleClass('active');
            $('.menu').toggleClass('is-active');
        });
    }());
    // ナビメニューを開いた際、ページ背景固定
    $('#nav-toggle').on('click', function () {
        $('body').toggleClass('open');
    });
    $('#gloval-nav a[href]').on('click', function () {
        $('body').toggleClass('open');
    });
}); */
$(function () {
    // ナビメニュー開閉
    $('#nav-toggle').on('click', function () {
        $('body').toggleClass('open');
        $(this).toggleClass('active');
        $('.menu').toggleClass('is-active');
    });
    $('#gloval-nav a, li').on('click', function () {
        $('body').removeClass('open');
        $('#nav-toggle').removeClass('active');
        $('.menu').removeClass('is-active');
        $('.menu-list').css('display', 'none');
    });
});

// ヘッダーリンクのホバー
var parent = document.querySelectorAll(".has-sub");
var node = Array.prototype.slice.call(parent, 0);
node.forEach(function (element) {
    element.addEventListener(
        "mouseover",
        function () {
            $(this).addClass("active");
            element.querySelector(".sub").classList.add("active");
        },
        false
    );
    element.addEventListener(
        "mouseout",
        function () {
            $(this).removeClass("active");
            element.querySelector(".sub").classList.remove("active");
        },
        false
    );
});

// トップページのコンテンツリンクホバー
$(function () {
    $('#pageLinks li')
        // マウスポインターが画像に乗った時の動作
        .mouseover(function (e) {
            $(this).addClass("active");
        })
        // マウスポインターが画像から外れた時の動作
        .mouseout(function (e) {
            $(this).removeClass("active");
        });
});

$(window).on('load resize', function () {
    var winW = $(window).width();
    var devW = 1023;
    if (winW <= devW) {
        //1023px以下の時の処理
        var lHeader = document.getElementById("l-header");
        lHeader.remove();
    } else {
        //1024pxより大きい時の処理
        var sHeader = document.getElementById("s-header");
        sHeader.remove();
    }
});


var FSectionH = $('section:first-child').outerHeight(true);//最初のsectionの高さを取得

//スクロール途中からヘッダーを変化させるための設定を関数でまとめる
function FixedAnime() {
    //ヘッダーの高さを取得
    var scroll = $(window).scrollTop();
    if (scroll >= FSectionH) {//ヘッダーの高さを超えたら
        $('header').addClass('fixedHeader');//headerにfixedHeaderというクラス名を付与
    } else {
        $('header').removeClass('fixedHeader');//fixedHeaderというクラス名を除去
    }
}

// 画面をスクロールをしたら動かしたい場合の記述
$(window).scroll(function () {
    FixedAnime();
});

// メニュー内コンテンツ開閉
$(document).ready(function () {
    $('.menu-tab').click(function () {
        $(this).next('.menu-list').slideToggle();
        $(this).toggleClass('menu-open');
    });
});

// スムーススクロール
$('a[href^="#"]').click(function () {
    var speed = 500;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top - 80;
    $("html, body").animate({
        scrollTop: position
    }, speed, "swing");
    return false;
});