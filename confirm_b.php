<?php
if (isset($_POST['area']) && is_array($_POST['area'])) {
    $area = implode("、", $_POST["area"]);
}
if (isset($_POST['propertytype']) && is_array($_POST['propertytype'])) {
    $propertytype = implode("、", $_POST["propertytype"]);
}
if (isset($_POST['floor']) && is_array($_POST['floor'])) {
    $floor = implode("、", $_POST["floor"]);
}
if (isset($_POST['conditions']) && is_array($_POST['conditions'])) {
    $conditions = implode("、", $_POST["conditions"]);
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $route1 = $_POST["route1"];
    $route2 = $_POST["route2"];
    $route3 = $_POST["route3"];
    $station1 = $_POST["station1"];
    $station2 = $_POST["station2"];
    $station3 = $_POST["station3"];
    $breadth = $_POST["breadth"];
    $breadthEnd = $_POST["breadthEnd"];
    $PSname = $_POST["PSname"];
    $profession = $_POST["profession"];
    $inSeason = $_POST["inSeason"];
    $PSmobilenumber = $_POST["PSmobilenumber"];
    $PSmailaddress = $_POST["PSmailaddress"];
    $builtAge = $_POST["builtAge"];
    $freetext = $_POST["freetext"];
}
if (isset($_POST["submit"])) {
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    $subject = "【HPよりお客様からお問い合わせが有りました。】";
    $body = <<< EOM
{$PSname}　様
からお問い合わせが有りました。
===================================================

■ ■ 物件リクエスト お客様情報

【 お名前 】
    {$PSname}

【　職業 】
    {$profession}

【 入居時期 】
    {$inSeason}

【 携帯電話番号 】
    {$PSmobilenumber}

【 メールアドレス 】
    {$PSmailaddress}

    == 物件リクエスト ==

【路線・駅】
{$route1}
{$station1}
{$route2}
{$station2}
{$route3}
{$station3}

【エリア】
$_POST[area]

【物件種別】
$_POST[propertytype]

【広さ】
{$breadth} 〜 {$breadthEnd}

【間取り】
$_POST[floor]

【築年数】
{$builtAge}

【条件】
$_POST[conditions]

【その他ご要望】
{$freetext}


===================================================

EOM;
    $fromEmail = "info@sky-yard.co.jp";
    $fromName = "SkyYardProperty【物件リクエスト】";
    $header = "From: " . mb_encode_mimeheader($fromName) . "<{$fromEmail}>";
    $ReturnPath = "info@sky-yard.co.jp";
    mb_send_mail($email, $subject, $body, $header, $ReturnPath);
    header("Location: http://testacsv.xyz/testsypform/thanks.html");
    exit;
}
?>

<!DOCTYPE html>
<html lang="ja-JP">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover,user-scalable=no">
    <link rel="stylesheet" href="css/style.min.css">
    <title>物件リクエスト | SkyYardProperty株式会社</title>
    <meta name="description" content="港区を中心とした都内のタワーマンション・高級マンションの販売・仲介・賃貸管理等を行っております。高級物件に特化した知識を持つスタッフのみ在籍。不動産売買・物件購入ならSkyYardProperty株式会社にお任せください。">
    <meta name="keywords" content="SkyYardProperty株式会社,港区,タワーマンション,マンション,アパート,購入,販売,物件,高級,不動産売買,物件購入">
    <meta property="og:site_name" content="SkyYardProperty株式会社">
    <meta property="og:title" content="物件リクエスト | SkyYardProperty株式会社">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:description" content="港区を中心とした都内のタワーマンション・高級マンションの販売・仲介・賃貸管理等を行っております。高級物件に特化した知識を持つスタッフのみ在籍。不動産売買・物件購入ならSkyYardProperty株式会社にお任せください。">
    <link rel="icon" type="image/svg+xml" href="img/favicon.svg">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+JP:wght@500;600;700&display=swap" rel="stylesheet">
</head>

<body>
    <header>
        <div class="content">
            <div class="wrapper">
                <a href="index.html" class="logo">
                    <img src="img/logo.svg" alt="SkyYardProperty株式会社">
                </a>
                <nav id="s-header" class="s-header max1024">
                    <div id="nav-toggle" class="nav-toggle"><span></span><span></span></div>
                    <div class="menu">
                        <div>
                            <a href="index.html">
                                <p>HOME</p><span>ホーム</span>
                            </a>
                        </div>
                        <div>
                            <a tabindex="-1" class="menu-tab menu-tab-corporate">
                                <p>CORPORATE</p><span>企業概要</span>
                            </a>
                            <ul class="menu-list">
                                <li><a href="corporate.html">企業概要トップ</a></li>
                                <li><a href="corporate.html#topMessage">代表挨拶</a></li>
                                <li><a href="corporate.html#company">会社概要</a></li>
                                <li><a href="corporate.html#access">access</a></li>
                                <li><a href="corporate.html#member">役員紹介</a></li>
                                <li><a href="corporate.html#history">会社沿革</a></li>
                            </ul>
                        </div>
                        <div>
                            <a href="service.html">
                                <p>SERVICE</p><span>業務内容</span>
                            </a>
                        </div>
                        <div>
                            <a href="results.html">
                                <p>RESULTS</p><span>取り扱い実績</span>
                            </a>
                        </div>
                        <div>
                            <a href="recruit.html">
                                <p>RECRUIT</p><span>採用</span>
                            </a>
                        </div>
                        <div>
                            <a href="contact.html">
                                <p>CONTACT</p><span>お問い合わせ</span>
                            </a>
                        </div>
                    </div>
                </nav>
                <nav id="l-header" class="l-header min1024">
                    <ul class="nav">
                        <li class="has-sub">
                            <a href="corporate.html">
                                <p>CORPORATE</p>
                                <span>企業概要</span>
                            </a>
                            <ul class="sub">
                                <li><a href="corporate.html#topMessage">代表挨拶</a></li>
                                <li><a href="corporate.html#company">会社概要</a></li>
                                <li><a href="corporate.html#access">access</a></li>
                                <li><a href="corporate.html#member">役員紹介</a></li>
                                <li><a href="corporate.html#history">会社沿革</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="service.html">
                                <p>SERVICE</p>
                                <span>事業内容</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="results.html">
                                <p>RESULTS</p>
                                <span>取り扱い実績</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="recruit.html">
                                <p>RECRUIT</p>
                                <span>採用</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="contact.html">
                                <p>CONTACT</p>
                                <span>お問い合わせ</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <section id="contact01-hero" class="section contact01-hero hero">
            <div class="hero-wrapper">
                <p>物件リクエスト</p>
            </div>
        </section>
        <section id="contact" class="section contact">
            <div class="form_wrap">
                <form action="confirm_b.php" method="post">
                    <input type="hidden" name="route1" value="<?php echo $route1; ?>">
                    <input type="hidden" name="route2" value="<?php echo $route2; ?>">
                    <input type="hidden" name="route3" value="<?php echo $route3; ?>">
                    <input type="hidden" name="station1" value="<?php echo $station1; ?>">
                    <input type="hidden" name="station2" value="<?php echo $station2; ?>">
                    <input type="hidden" name="station3" value="<?php echo $station3; ?>">
                    <input type="hidden" name="profession" value="<?php echo $profession; ?>">
                    <input type="hidden" name="PSname" value="<?php echo $PSname; ?>">
                    <input type="hidden" name="PSmobilenumber" value="<?php echo $PSmobilenumber; ?>">
                    <input type="hidden" name="PSmailaddress" value="<?php echo $PSmailaddress; ?>">
                    <input type="hidden" name="freetext" value="<?php echo $freetext; ?>">
                    <input type="hidden" name="inSeason" value="<?php echo $inSeason; ?>">
                    <input type="hidden" name="floor" value="<?php echo $floor; ?>">
                    <input type="hidden" name="breadth" value="<?php echo $breadth; ?>">
                    <input type="hidden" name="breadthEnd" value="<?php echo $breadthEnd; ?>">
                    <input type="hidden" name="builtAge" value="<?php echo $builtAge; ?>">
                    <input type="hidden" name="propertytype" value="<?php echo $propertytype; ?>">
                    <input type="hidden" name="area" value="<?php echo $area; ?>">
                    <input type="hidden" name="conditions" value="<?php echo $conditions; ?>">
                    <h3>お問い合わせ 内容確認</h3>
                    <p>記入事項にお間違い無いかご確認の上、<br>
                        「送信する」ボタンを押して下さい。</p>
                    <div>
                        <h3>物件リクエスト 内容確認</h3>
                        <h5>路線・駅</h5>
                        <?php echo $route1; ?>
                        <?php echo $station1; ?>
                        <?php echo $route2; ?>
                        <?php echo $station2; ?>
                        <?php echo $route3; ?>
                        <?php echo $station3; ?>

                        <h5>エリア</h5>
                        <?php foreach ($_POST[area] as $value) {
                            echo "{$value}, ";
                        } ?>
                        <h5> 物件種別 </h5>
                        <?php foreach ($_POST[propertytype] as $value) {
                            echo "{$value}, ";
                        } ?>
                        <h5> 広さ </h5>
                        <?php echo $breadth; ?> 〜 <?php echo $breadthEnd; ?>

                        <h5> 間取り </h5>
                        <?php foreach ($_POST[floor] as $value) {
                            echo "{$value}, ";
                        } ?>
                        <h5> 築年数 </h5>
                        <?php echo $builtAge; ?>

                        <h5> 条件 </h5>
                        <?php foreach ($_POST[conditions] as $value) {
                            echo "{$value}, ";
                        } ?>
                        <h3>お客様情報</h3>
                        <h5> お名前 </h5>
                        <?php echo $PSname; ?>

                        <h5> ご職業 </h5>
                        <?php echo $profession; ?>

                        <h5> 入居時期 </h5>
                        <?php echo $inSeason; ?>

                        <h5> 携帯電話番号 </h5>
                        <?php echo $PSmobilenumber; ?>

                        <h5> メールアドレス </h5>
                        <?php echo $PSmailaddress; ?>

                        <h5> その他ご要望 </h5>
                        <?php echo $freetext; ?>

                    </div>
                    <input class="backbtn" type="button" value="内容を修正する" onclick="history.back(-1)">
                    <button type="submit" name="submit" class="submit_btn">送信する</button>
                </form>
            </div>
        </section>

    </main>
    <footer>
        <div class="inner">
            <a href="index.html" class="logo">
                <img src="img/logoL.svg" alt="SkyYardProperty株式会社">
            </a>
            <div class="footer_contents">
                <div class="footer_linkGroup">
                    <p class="title">Address</p>
                    <p>〒105-0004<br>東京都港区新橋3-1-11<br>長友ランディックビル 6階</p>
                </div>
                <div class="footer_linkGroup">
                    <p class="title">Contacts</p>
                    <p><a href="contact.html">Mail:info@sky-yard.co.jp</a></p>
                    <a href="tel:0366722471">Tel:03-6672-2471</a>
                    <p>Fax:03-6672-7638</p>
                </div>
                <div class="footer_linkGroup link">
                    <p class="title">Links</p>
                    <div>
                        <a href="corporate.html">企業概要</a>
                        <a href="service.html">事業内容</a>
                        <a href="results.html">取り扱い実績</a>
                        <a href="recruit.html">採用</a>
                        <a href="contact.html">お問い合わせ</a>
                        <a href="privacy.html">利用規約</a>
                    </div>
                </div>
            </div>
            <div class="bnr_wrap">
                <a class="suumoBnr" href="https://suumo.jp/chintai/kaisha/kc_030_003511000/" target="_blank">
                    <img src="img/suumo_bnr.jpg" alt="SUUMOに物件掲載中。こちらからスーモで物件をチェックする">
                </a>
                <a class="requestBnr" href="contact.html">
                    <img src="img/request_bnr.jpg" alt="物件リクエスト受付中！SUUMOに掲載していない物件も対応可能。物件を指定してお探しの方はこちらから">
                </a>
            </div>
        </div>
        <small>&copy; 2022 SkyYardProperty Inc.</small>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</body>

</html>