<?php
session_start();
$upload_dir = '/virtual/syp/public_html/www.sky-yard.co.jp/upload/';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $person = $_POST["person"];
    $corporatename = $_POST["corporatename"];
    $nameSei = $_POST["nameSei"];
    $nameMei = $_POST["nameMei"];
    $furiganaSei = $_POST["furiganaSei"];
    $furiganaMei = $_POST["furiganaMei"];
    $gender = $_POST["gender"];
    $PSyear = $_POST["PSyear"];
    $PSmonth = $_POST["PSmonth"];
    $PSday = $_POST["PSday"];
    $spouse = $_POST["spouse"];
    $PSpref = $_POST["PSpref"];
    $PSaddress1 = $_POST["PSaddress1"];
    $PSaddress2 = $_POST["PSaddress2"];
    $PSaddress3 = $_POST["PSaddress3"];
    $mobilenumber = $_POST["mobilenumber"];
    $phonenumber = $_POST["phonenumber"];
    $mailaddress = $_POST["mailaddress"];
    $income = $_POST["income"];
    $incometype = $_POST["incometype"];
    $livingstyle = $_POST["livingstyle"];
    $rent = $_POST["rent"];
    $livingyear = $_POST["livingyear"];
    $livingmonth = $_POST["livingmonth"];
    $reason = $_POST["reason"];
    $aim = $_POST["aim"];
    $profession = $_POST["profession"];
    $officename = $_POST["officename"];
    $officenamekana = $_POST["officenamekana"];
    $officemember = $_POST["officemember"];
    $officepref = $_POST["officepref"];
    $officeadreess1 = $_POST["officeadreess1"];
    $officeadreess2 = $_POST["officeadreess2"];
    $officeadreess3 = $_POST["officeadreess3"];
    $mainofficepref = $_POST["mainofficepref"];
    $mainofficeadreess1 = $_POST["mainofficeadreess1"];
    $mainofficeadreess2 = $_POST["mainofficeadreess2"];
    $mainofficeadreess3 = $_POST["mainofficeadreess3"];
    $mainofficenumber = $_POST["mainofficenumber"];
    $yearService = $_POST["yearService"];
    $monthService = $_POST["monthService"];
    $servicePosition = $_POST["servicePosition"];
    $industry = $_POST["industry"];
    $industryOther = $_POST["industryOther"];
    $resident = $_POST["resident"];
    $residentnumber = $_POST["residentnumber"];
    $income1nameSei = $_POST["income1nameSei"];
    $income1nameMei = $_POST["income1nameMei"];
    $income1furiganaSei = $_POST["income1furiganaSei"];
    $income1furiganaMei = $_POST["income1furiganaMei"];
    $income1gender = $_POST["income1gender"];
    $income1year = $_POST["income1year"];
    $income1month = $_POST["income1month"];
    $income1day = $_POST["income1day"];
    $add1relationship = $_POST["add1relationship"];
    $add1nameSei = $_POST["add1nameSei"];
    $add1nameMei = $_POST["add1nameMei"];
    $add1furiganaSei = $_POST["add1furiganaSei"];
    $add1furiganaMei = $_POST["add1furiganaMei"];
    $add1year = $_POST["add1year"];
    $add1month = $_POST["add1month"];
    $add1day = $_POST["add1day"];
    $add2relationship = $_POST["add2relationship"];
    $add2nameSei = $_POST["add2nameSei"];
    $add2nameMei = $_POST["add2nameMei"];
    $add2furiganaSei = $_POST["add2furiganaSei"];
    $add2furiganaMei = $_POST["add2furiganaMei"];
    $add2gender = $_POST["add2gender"];
    $add2year = $_POST["add2year"];
    $add2month = $_POST["add2month"];
    $add2day = $_POST["add2day"];
    $add3relationship = $_POST["add3relationship"];
    $add3nameSei = $_POST["add3nameSei"];
    $add3nameMei = $_POST["add3nameMei"];
    $add3furiganaSei = $_POST["add3furiganaSei"];
    $add3furiganaMei = $_POST["add3furiganaMei"];
    $add3gender = $_POST["add3gender"];
    $add3year = $_POST["add3year"];
    $add3month = $_POST["add3month"];
    $add3day = $_POST["add3day"];
    $add4relationship = $_POST["add4relationship"];
    $add4nameSei = $_POST["add4nameSei"];
    $add4nameMei = $_POST["add4nameMei"];
    $add4furiganaSei = $_POST["add4furiganaSei"];
    $add4furiganaMei = $_POST["add4furiganaMei"];
    $add4gender = $_POST["add4gender"];
    $add4year = $_POST["add4year"];
    $add4month = $_POST["add4month"];
    $add4day = $_POST["add4day"];
    $add5relationship = $_POST["add5relationship"];
    $add5nameSei = $_POST["add5nameSei"];
    $add5nameMei = $_POST["add5nameMei"];
    $add5furiganaSei = $_POST["add5furiganaSei"];
    $add5furiganaMei = $_POST["add5furiganaMei"];
    $add5gender = $_POST["add5gender"];
    $add5year = $_POST["add5year"];
    $add5month = $_POST["add5month"];
    $add5day = $_POST["add5day"];
    $ECRelation = $_POST["ECRelation"];
    $ECnameSei = $_POST["ECnameSei"];
    $ECnameMei = $_POST["ECnameMei"];
    $ECfuriganaSei = $_POST["ECfuriganaSei"];
    $ECfuriganaMei = $_POST["ECfuriganaMei"];
    $ECgender = $_POST["ECgender"];
    $ECyear = $_POST["ECyear"];
    $ECmonth = $_POST["ECmonth"];
    $ECday = $_POST["ECday"];
    $ECadreess1 = $_POST["ECadreess1"];
    $ECadreess2 = $_POST["ECadreess2"];
    $ECadreess3 = $_POST["ECadreess3"];
    $mobilenumber = $_POST["mobilenumber"];
    $file = $_FILES['pctfile1']['tmp_name'];
    $file2 = $_FILES['pctfile2']['tmp_name'];
}

if (isset($_POST["submit"])) {
    mb_language("ja");
    mb_internal_encoding("UTF-8");
    $subject = "【HPよりお客様からお問い合わせが有りました。】";
    $body = <<< EOM
{$nameSei} {$nameMei}　様
からお問い合わせが有りました。
===================================================

■ ■ お申し込みお客様情報\n\n【個人/法人】\n    {$person}\n\n【契約名義となる法人名】\n    {$corporatename}\n\n【 お名前 】\n    {$nameSei} {$nameMei}\n\n【 フリガナ 】\n    {$furiganaSei} {$furiganaMei}\n\n【 性別 】\n    {$gender}\n\n【 生年月日 】\n    {$PSyear}年　{$PSmonth}月 {$PSday}日\n\n【 配偶者 】\n    {$spouse}\n\n【 現住所 】\n    {$PSpref}\n    {$PSaddress1}{$PSaddress2}{$PSaddress3}\n\n【 携帯電話番号 】\n    {$mobilenumber}\n\n【 固定電話番号 】\n    {$phonenumber}\n\n【 メールアドレス 】\n    {$mailaddress}\n\n【 年収 】\n    {$income} 万円\n\n【年収種別】\n    {$incometype}\n\n\n■ ■ 現住居の居住形態\n\n【居住携帯】\n    {$livingstyle}\n\n【 現在の賃料 】(賃貸の場合)\n    {$rent} 万円\n\n【 居住年数 】\n    {$livingyear} 年 {$livingmonth}\n\n【 引越し理由 】\n    {$reason}\n\n【 使用目的 】\n    {$aim}\n\n■ ■ 勤務先情報\n\n【 職業 】\n    {$profession}\n\n【 勤務先名 】\n    {$officename}\n\n【 勤務先カナ 】\n    {$officenamekana}\n\n【 従業員数 】\n    {$officemember} 人\n\n【 勤務先所在地 】\n    {$officepref}\n    {$officeadreess1}\n    {$officeadreess2}\n    {$officeadreess3}\n\n■ ■ 本所所在地\n【{$mainofficesame}(勤務先と異なる場合は記入)】\n\n\n【 本所所在地住所 】\n    {$mainofficepref}\n    {$mainofficeadreess1}\n    {$mainofficeadreess2}\n    {$mainofficeadreess3}\n\n【 本社電話番号 】\n    {$mainofficenumbe}\n\n【 勤続年数 】\n    {$yearService} 年{$monthService} 月\n\n【 所属役職 】\n    {$servicePosition}\n\n【 業種 】\n    {$industry}\n    (その他の場合){$industryOther}\n\n【 入居者種別 】\n    {$resident}\n\n【 入居者人数 】\n    {$residentnumber} 人\n\n\n■ ■ 入居者(1)\n申込者本人のみ／申込者及び同居人を選ぶと申込者情報が反映されます。\n\n【続柄】\n    {$relationship}\n\n【 お名前 】\n    {$income1nameSei} {$income1nameMei}\n\n【 フリガナ 】\n    {$income1furiganaSei} {$income1furiganaMei}\n\n【 性別 】\n    {$income1gender}\n\n【 生年月日 】\n    {$income1year}年 {$income1month}月 {$income1day}日\n\n\n■ ■ 追加入居者(1)\n【 続柄 】\n    {$add1relationship}\n\n【 お名前 】\n    {$add1nameSei} {$add1nameMei}\n\n【 フリガナ 】\n    {$add1furiganaSei} {$add1furiganaMei}\n\n【 性別 】\n    {$add1gender}\n\n【 生年月日 】\n    {$add1year}年　{$add1month}月 {$add1day}日\n\n\n■ ■ 追加入居者(2)\n【 続柄 】\n    {$add2relationship}\n\n【 お名前 】\n    {$add2nameSei} {$add2nameMei}\n\n【 フリガナ 】\n    {$add2furiganaSei} {$add2furiganaMei}\n\n【 性別 】\n    {$add2gender}\n【 生年月日 】\n    {$add2year}年　{$add2month}月 {$add2day}日\n\n\n■ ■ 追加入居者(3)\n【 続柄 】\n    {$add3relationship}\n\n【 お名前 】\n    {$add3nameSei} {$add3nameMei}\n\n【 フリガナ 】\n    {$add3furiganaSei} {$add3furiganaMei}\n\n【 性別 】\n    {$add3gender}\n\n【 生年月日 】\n    {$add3year}年　{$add3month}月 {$add3day}日\n\n\n■ ■ 追加入居者(4)\n【 続柄 】\n    {$add4relationship}\n\n【 お名前 】\n    {$add4nameSei} {$add4nameMei}\n\n【 フリガナ 】\n    {$add4furiganaSei} {$add4furiganaMei}\n\n【 性別 】\n    {$add4gender}\n\n【 生年月日 】\n    {$add4year}年　{$add4month}月 {$add4day}日\n\n\n■ ■ 追加入居者(5)\n【 続柄 】\n    {$add5relationship}\n\n【 お名前 】\n    {$add5nameSei} {$add5nameMei}\n\n【 フリガナ 】\n    {$add5furiganaSei} {$add5furiganaMei}\n\n【 性別 】\n    {$add5gender}\n\n【 生年月日 】\n    {$add5year}年　{$add5month}月 {$add5day}日\n\n\n■ ■ 緊急連絡先\n\n【 続柄 】\n    {$ECRelation}\n\n【 お名前 】\n    {$ECnameSei} {$ECnameMei}\n\n【 フリガナ 】\n    {$ECfuriganaSei} {$ECfuriganaMei}\n\n【 性別 】\n    {$ECgender}\n\n【 生年月日 】\n    {$ECyear}年　{$ECmonth}月 {$ECday}日\n\n【 現住所 】\n{$ECpref}\n{$ECadreess1}\n{$ECadreess2}\n{$ECadreess3}\n\n【 携帯電話番号 】\n    {$ECmobilenumber}\n\n【 固定電話番号 】\n    {$ECphonenumber}\n\n【本人確認書類(1)】\n{$_SESSION['file']}\n【本人確認書類(2)】\n{$_SESSION['file2']}\n
===================================================

EOM;
    $fromEmail = "info@sky-yard.co.jp";
    //$ReturnPath = "info@sky-yard.co.jp";
    $email = "info@sky-yard.co.jp";
    $fromName = "お問い合わせ";
    $header = "Content-Type: multipart/mixed;boundary=\"__BOUNDARY__\"\n";
    $header .= "From: " . mb_encode_mimeheader($fromName) . "<{$fromEmail}>\n";
    // 本文
    $sendBody = "--__BOUNDARY__\n";
    $sendBody .= "Content-Type: text/plain; charset=\"ISO-2022-JP\"\n\n";
    $sendBody .= $body . "\n";
    $sendBody .= "--__BOUNDARY__\n";
    // 添付1(name="pcfile1")
    $sendBody .= "Content-Type: application/octet-stream; name=\"{$_SESSION['file']}\"\n";
    $sendBody .= "Content-Disposition: attachment; filename=\"{$_SESSION['file']}\"\n";
    $sendBody .= "Content-Transfer-Encoding: base64\n";
    $sendBody .= "\n";
    $sendBody .= chunk_split(base64_encode(file_get_contents($upload_dir . $_SESSION['file'])));
    $sendBody .= "--__BOUNDARY__\n";
    // 添付2(name="pcfile2")
    $sendBody .= "Content-Type: application/octet-stream; name=\"{$_SESSION['file2']}\"\n";
    $sendBody .= "Content-Disposition: attachment; filename=\"{$_SESSION['file2']}\"\n";
    $sendBody .= "Content-Transfer-Encoding: base64\n";
    $sendBody .= "\n";
    $sendBody .= chunk_split(base64_encode(file_get_contents($upload_dir . $_SESSION['file2'])));
    $sendBody .= "--__BOUNDARY__\n";

    mb_send_mail($email, $subject, $sendBody, $header);
    header("Location: http://www.sky-yard.co.jp/thanks.html");
    exit;
    // 確認フォーム表示の場合
} else {
    // formから送られてきた画像の一時アップロード
    if (!empty($file) && !empty($file2)) {
        move_uploaded_file($_FILES['pctfile1']['tmp_name'], $upload_dir . $_FILES['pctfile1']['name']);
        move_uploaded_file($_FILES['pctfile2']['tmp_name'], $upload_dir . $_FILES['pctfile2']['name']);
        $_SESSION['file'] = $_FILES['pctfile1']['name'];
        $_SESSION['file2'] = $_FILES['pctfile2']['name'];
    }
}
?>

<!DOCTYPE html>
<html lang="ja-JP">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover,user-scalable=no">
    <link rel="stylesheet" href="css/style.min.css">
    <title>お問い合わせ | SkyYardProperty株式会社</title>
    <meta name="description" content="港区を中心とした都内のタワーマンション・高級マンションの販売・仲介・賃貸管理等を行っております。高級物件に特化した知識を持つスタッフのみ在籍。不動産売買・物件購入ならSkyYardProperty株式会社にお任せください。">
    <meta name="keywords" content="SkyYardProperty株式会社,港区,タワーマンション,マンション,アパート,購入,販売,物件,高級,不動産売買,物件購入">
    <meta property="og:site_name" content="SkyYardProperty株式会社">
    <meta property="og:title" content="お問い合わせ | SkyYardProperty株式会社">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="ja_JP">
    <meta property="og:description" content="港区を中心とした都内のタワーマンション・高級マンションの販売・仲介・賃貸管理等を行っております。高級物件に特化した知識を持つスタッフのみ在籍。不動産売買・物件購入ならSkyYardProperty株式会社にお任せください。">
    <link rel="icon" type="image/svg+xml" href="img/favicon.svg">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+JP:wght@500;600;700&display=swap" rel="stylesheet">
</head>

<body>
    <header>
        <div class="content">
            <div class="wrapper">
                <a href="index.html" class="logo">
                    <img src="img/logo.svg" alt="SkyYardProperty株式会社">
                </a>
                <nav id="s-header" class="s-header max1024">
                    <div id="nav-toggle" class="nav-toggle"><span></span><span></span></div>
                    <div class="menu">
                        <div>
                            <a href="index.html">
                                <p>HOME</p><span>ホーム</span>
                            </a>
                        </div>
                        <div>
                            <a tabindex="-1" class="menu-tab menu-tab-corporate">
                                <p>CORPORATE</p><span>企業概要</span>
                            </a>
                            <ul class="menu-list">
                                <li><a href="corporate.html">企業概要トップ</a></li>
                                <li><a href="corporate.html#topMessage">代表挨拶</a></li>
                                <li><a href="corporate.html#company">会社概要</a></li>
                                <li><a href="corporate.html#access">access</a></li>
                                <li><a href="corporate.html#member">役員紹介</a></li>
                                <li><a href="corporate.html#history">会社沿革</a></li>
                            </ul>
                        </div>
                        <div>
                            <a href="service.html">
                                <p>SERVICE</p><span>業務内容</span>
                            </a>
                        </div>
                        <div>
                            <a href="results.html">
                                <p>RESULTS</p><span>取り扱い実績</span>
                            </a>
                        </div>
                        <div>
                            <a href="recruit.html">
                                <p>RECRUIT</p><span>採用</span>
                            </a>
                        </div>
                        <div>
                            <a href="contact.html">
                                <p>CONTACT</p><span>お問い合わせ</span>
                            </a>
                        </div>
                    </div>
                </nav>
                <nav id="l-header" class="l-header min1024">
                    <ul class="nav">
                        <li class="has-sub">
                            <a href="corporate.html">
                                <p>CORPORATE</p>
                                <span>企業概要</span>
                            </a>
                            <ul class="sub">
                                <li><a href="corporate.html#topMessage">代表挨拶</a></li>
                                <li><a href="corporate.html#company">会社概要</a></li>
                                <li><a href="corporate.html#access">access</a></li>
                                <li><a href="corporate.html#member">役員紹介</a></li>
                                <li><a href="corporate.html#history">会社沿革</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="service.html">
                                <p>SERVICE</p>
                                <span>事業内容</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="results.html">
                                <p>RESULTS</p>
                                <span>取り扱い実績</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="recruit.html">
                                <p>RECRUIT</p>
                                <span>採用</span>
                            </a>
                        </li>
                        <li class="has-sub">
                            <a href="contact.html">
                                <p>CONTACT</p>
                                <span>お問い合わせ</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <main>
        <section id="contact01-hero" class="section contact01-hero hero">
            <div class="hero-wrapper">
                <p>お申し込みを<br class="max1024">ご検討のお客様</p>
            </div>
        </section>
        <section id="contact" class="section contact">
            <div class="form_wrap">
                <form action="confirm_a.php" method="post">
                    <input type="hidden" name="person" value="<?php echo $person; ?>">
                    <input type="hidden" name="corporatename" value="<?php echo $corporatename; ?>">
                    <input type="hidden" name="nameSei" value="<?php echo $nameSei; ?>">
                    <input type="hidden" name="nameMei" value="<?php echo $nameMei; ?>">
                    <input type="hidden" name="furiganaSei" value="<?php echo $furiganaSei; ?>">
                    <input type="hidden" name="furiganaMei" value="<?php echo $furiganaMei; ?>">
                    <input type="hidden" name="gender" value="<?php echo $gender; ?>">
                    <input type="hidden" name="PSyear" value="<?php echo $PSyear; ?>">
                    <input type="hidden" name="PSmonth" value="<?php echo $PSmonth; ?>">
                    <input type="hidden" name="PSday" value="<?php echo $PSday; ?>">
                    <input type="hidden" name="spouse" value="<?php echo $spouse; ?>">
                    <input type="hidden" name="PSpref" value="<?php echo $PSpref; ?>">
                    <input type="hidden" name="PSaddress1" value="<?php echo $PSaddress1; ?>">
                    <input type="hidden" name="PSaddress2" value="<?php echo $PSaddress2; ?>">
                    <input type="hidden" name="PSaddress3" value="<?php echo $PSaddress3; ?>">
                    <input type="hidden" name="mobilenumber" value="<?php echo $mobilenumber; ?>">
                    <input type="hidden" name="phonenumber" value="<?php echo $phonenumber; ?>">
                    <input type="hidden" name="mailaddress" value="<?php echo $mailaddress; ?>">
                    <input type="hidden" name="income" value="<?php echo $income; ?>">
                    <input type="hidden" name="incometype" value="<?php echo $incometype; ?>">
                    <input type="hidden" name="livingstyle" value="<?php echo $livingstyle; ?>">
                    <input type="hidden" name="rent" value="<?php echo $rent; ?>">
                    <input type="hidden" name="livingyear" value="<?php echo $livingyear; ?>">
                    <input type="hidden" name="livingmonth" value="<?php echo $livingmonth; ?>">
                    <input type="hidden" name="reason" value="<?php echo $reason; ?>">
                    <input type="hidden" name="aim" value="<?php echo $aim; ?>">
                    <input type="hidden" name="profession" value="<?php echo $profession; ?>">
                    <input type="hidden" name="officename" value="<?php echo $officename; ?>">
                    <input type="hidden" name="officenamekana" value="<?php echo $officenamekana; ?>">
                    <input type="hidden" name="officemember" value="<?php echo $officemember; ?>">
                    <input type="hidden" name="officepref" value="<?php echo $officepref; ?>">
                    <input type="hidden" name="officeadreess1" value="<?php echo $officeadreess1; ?>">
                    <input type="hidden" name="officeadreess2" value="<?php echo $officeadreess2; ?>">
                    <input type="hidden" name="officeadreess3" value="<?php echo $officeadreess3; ?>">
                    <input type="hidden" name="mainofficepref" value="<?php echo $mainofficepref; ?>">
                    <input type="hidden" name="mainofficeadreess1" value="<?php echo $mainofficeadreess1; ?>">
                    <input type="hidden" name="mainofficeadreess2" value="<?php echo $mainofficeadreess2; ?>">
                    <input type="hidden" name="mainofficeadreess3" value="<?php echo $mainofficeadreess3; ?>">
                    <input type="hidden" name="mainofficenumber" value="<?php echo $mainofficenumber; ?>">
                    <input type="hidden" name="yearService" value="<?php echo $yearService; ?>">
                    <input type="hidden" name="monthService" value="<?php echo $monthService; ?>">
                    <input type="hidden" name="servicePosition" value="<?php echo $servicePosition; ?>">
                    <input type="hidden" name="industry" value="<?php echo $industry; ?>">
                    <input type="hidden" name="industryOther" value="<?php echo $industryOther; ?>">
                    <input type="hidden" name="resident" value="<?php echo $resident; ?>">
                    <input type="hidden" name="residentnumber" value="<?php echo $residentnumber; ?>">
                    <input type="hidden" name="income1nameSei" value="<?php echo $income1nameSei; ?>">
                    <input type="hidden" name="income1nameMei" value="<?php echo $income1nameMei; ?>">
                    <input type="hidden" name="income1furiganaSei" value="<?php echo $income1furiganaSei; ?>">
                    <input type="hidden" name="income1furiganaMei" value="<?php echo $income1furiganaMei; ?>">
                    <input type="hidden" name="income1gender" value="<?php echo $income1gender; ?>">
                    <input type="hidden" name="income1year" value="<?php echo $income1year; ?>">
                    <input type="hidden" name="income1month" value="<?php echo $income1month; ?>">
                    <input type="hidden" name="income1day" value="<?php echo $income1day; ?>">
                    <input type="hidden" name="add1relationship" value="<?php echo $add1relationship; ?>">
                    <input type="hidden" name="add1nameSei" value="<?php echo $add1nameSei; ?>">
                    <input type="hidden" name="add1nameMei" value="<?php echo $add1nameMei; ?>">
                    <input type="hidden" name="add1furiganaSei" value="<?php echo $add1furiganaSei; ?>">
                    <input type="hidden" name="add1furiganaMei" value="<?php echo $add1furiganaMei; ?>">
                    <input type="hidden" name="add1year" value="<?php echo $add1year; ?>">
                    <input type="hidden" name="add1month" value="<?php echo $add1month; ?>">
                    <input type="hidden" name="add1day" value="<?php echo $add1day; ?>">
                    <input type="hidden" name="add2relationship" value="<?php echo $add2relationship; ?>">
                    <input type="hidden" name="add2nameSei" value="<?php echo $add2nameSei; ?>">
                    <input type="hidden" name="add2nameMei" value="<?php echo $add2nameMei; ?>">
                    <input type="hidden" name="add2furiganaSei" value="<?php echo $add2furiganaSei; ?>">
                    <input type="hidden" name="add2furiganaMei" value="<?php echo $add2furiganaMei; ?>">
                    <input type="hidden" name="add2gender" value="<?php echo $add2gender; ?>">
                    <input type="hidden" name="add2year" value="<?php echo $add2year; ?>">
                    <input type="hidden" name="add2month" value="<?php echo $add2month; ?>">
                    <input type="hidden" name="add2day" value="<?php echo $add2day; ?>">
                    <input type="hidden" name="add3relationship" value="<?php echo $add3relationship; ?>">
                    <input type="hidden" name="add3nameSei" value="<?php echo $add3nameSei; ?>">
                    <input type="hidden" name="add3nameMei" value="<?php echo $add3nameMei; ?>">
                    <input type="hidden" name="add3furiganaSei" value="<?php echo $add3furiganaSei; ?>">
                    <input type="hidden" name="add3furiganaMei" value="<?php echo $add3furiganaMei; ?>">
                    <input type="hidden" name="add3gender" value="<?php echo $add3gender; ?>">
                    <input type="hidden" name="add3year" value="<?php echo $add3year; ?>">
                    <input type="hidden" name="add3month" value="<?php echo $add3month; ?>">
                    <input type="hidden" name="add3day" value="<?php echo $add3day; ?>">
                    <input type="hidden" name="add4relationship" value="<?php echo $add4relationship; ?>">
                    <input type="hidden" name="add4nameSei" value="<?php echo $add4nameSei; ?>">
                    <input type="hidden" name="add4nameMei" value="<?php echo $add4nameMei; ?>">
                    <input type="hidden" name="add4furiganaSei" value="<?php echo $add4furiganaSei; ?>">
                    <input type="hidden" name="add4furiganaMei" value="<?php echo $add4furiganaMei; ?>">
                    <input type="hidden" name="add4gender" value="<?php echo $add4gender; ?>">
                    <input type="hidden" name="add4year" value="<?php echo $add4year; ?>">
                    <input type="hidden" name="add4month" value="<?php echo $add4month; ?>">
                    <input type="hidden" name="add4day" value="<?php echo $add4day; ?>">
                    <input type="hidden" name="add5relationship" value="<?php echo $add5relationship; ?>">
                    <input type="hidden" name="add5nameSei" value="<?php echo $add5nameSei; ?>">
                    <input type="hidden" name="add5nameMei" value="<?php echo $add5nameMei; ?>">
                    <input type="hidden" name="add5furiganaSei" value="<?php echo $add5furiganaSei; ?>">
                    <input type="hidden" name="add5furiganaMei" value="<?php echo $add5furiganaMei; ?>">
                    <input type="hidden" name="add5gender" value="<?php echo $add5gender; ?>">
                    <input type="hidden" name="add5year" value="<?php echo $add5year; ?>">
                    <input type="hidden" name="add5month" value="<?php echo $add5month; ?>">
                    <input type="hidden" name="add5day" value="<?php echo $add5day; ?>">
                    <input type="hidden" name="ECRelation" value="<?php echo $ECRelation; ?>">
                    <input type="hidden" name="ECnameSei" value="<?php echo $ECnameSei; ?>">
                    <input type="hidden" name="ECnameMei" value="<?php echo $ECnameMei; ?>">
                    <input type="hidden" name="ECfuriganaSei" value="<?php echo $ECfuriganaSei; ?>">
                    <input type="hidden" name="ECfuriganaMei" value="<?php echo $ECfuriganaMei; ?>">
                    <input type="hidden" name="ECgender" value="<?php echo $ECgender; ?>">
                    <input type="hidden" name="ECyear" value="<?php echo $ECyear; ?>">
                    <input type="hidden" name="ECmonth" value="<?php echo $ECmonth; ?>">
                    <input type="hidden" name="ECday" value="<?php echo $ECday; ?>">
                    <input type="hidden" name="ECadreess1" value="<?php echo $ECadreess1; ?>">
                    <input type="hidden" name="ECadreess2" value="<?php echo $ECadreess2; ?>">
                    <input type="hidden" name="ECadreess3" value="<?php echo $ECadreess3; ?>">
                    <input type="hidden" name="ECmobilenumber" value="<?php echo $ECmobilenumber; ?>">
                    <input type="hidden" name="ECphonenumber" value="<?php echo $ECphonenumber; ?>">
                    <h3>お問い合わせ 内容確認</h3>
                    <p>記入事項にお間違い無いかご確認の上、<br>
                        「送信する」ボタンを押して下さい。</p>
                    <div>
                        <h3>お申し込みお客様情報</h3>
                        <h5>個人/法人</h5>
                        <?php echo $person ?>

                        <h5>契約名義となる法人名</h5>
                        <?php echo $corporatename ?>

                        <h5> お名前 </h5>
                        <?php echo $nameSei; ?> <?php echo $nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $furiganaSei; ?> <?php echo $furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $PSyear; ?>年　<?php echo $PSmonth; ?>月 <?php echo $PSday; ?>日

                        <h5> 配偶者 </h5>
                        <?php echo $spouse; ?>

                        <h5> 現住所 </h5>
                        <?php echo $PSpref; ?>
                        <?php echo $PSaddress1; ?><?php echo $PSaddress2; ?><?php echo $PSaddress3; ?>

                        <h5> 携帯電話番号 </h5>
                        <?php echo $mobilenumber; ?>

                        <h5> 固定電話番号 </h5>
                        <?php echo $phonenumber; ?>

                        <h5> メールアドレス </h5>
                        <?php echo $mailaddress; ?>
                        <h5> 年収 </h5>
                        <?php echo $income; ?>

                        <h5>年収種別</h5>
                        <?php echo $incometype; ?>


                        <h3>現住居の居住形態</h3>

                        <h5>居住携帯</h5>
                        <?php echo $livingstyle; ?>

                        <h5> 現在の賃料 </h5>(賃貸の場合)
                        <?php echo $rent; ?>

                        <h5> 居住年数 </h5>
                        <?php echo $livingyear; ?>

                        <h5> 引越し理由 </h5>
                        <?php echo $reason; ?>

                        <h5> 使用目的 </h5>
                        <?php echo $aim; ?>

                        <h3>勤務先情報</h3>

                        <h5> 職業 </h5>
                        <?php echo $profession; ?>

                        <h5> 勤務先名 </h5>
                        <?php echo $officename; ?>

                        <h5> 勤務先カナ </h5>
                        <?php echo $officenamekana; ?>

                        <h5> 従業員数 </h5>
                        <?php echo $officemember; ?>

                        <h5> 勤務先所在地 </h5>
                        <?php echo $officepref; ?>
                        <?php echo $officeadreess1; ?>
                        <?php echo $officeadreess2; ?>
                        <?php echo $officeadreess3; ?>

                        <h3>本所所在地</h3>

                        勤務先と同じ
                        <h5><?php echo $mainofficesame; ?></h5>
                        (勤務先と異なる場合は記入)

                        <h5> 本所所在地住所 </h5>
                        <?php echo $mainofficepref; ?>
                        <?php echo $mainofficeadreess1; ?>
                        <?php echo $mainofficeadreess2; ?>
                        <?php echo $mainofficeadreess3; ?>

                        <h5> 本社電話番号 </h5>
                        <?php echo $mainofficenumbe; ?>

                        <h5> 勤続年数 </h5>
                        <?php echo $yearService; ?>年 <?php echo $monthService; ?>月

                        <h5> 所属役職 </h5>
                        <?php echo $servicePosition; ?>

                        <h5> 業種 </h5>
                        <?php echo $industry; ?>
                        <?php echo $industryOther; ?>

                        <h5> 入居者種別 </h5>
                        <?php echo $resident; ?>

                        <h5> 入居者人数 </h5>
                        <?php echo $residentnumber; ?>


                        <h3>入居者(1)</h3>
                        申込者本人のみ／申込者及び同居人を選ぶと申込者情報が反映されます。

                        <h5> お名前 </h5>
                        <?php echo $income1nameSei; ?> <?php echo $income1nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $income1furiganaSei; ?> <?php echo $income1furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $income1gender; ?>

                        <h5> 生年月日 </h5>
                        <?php echo $income1year; ?> <?php echo $income1month; ?>月 <?php echo $income1day; ?>日


                        <h3>追加入居者(1)</h3>
                        <h5> 続柄 </h5>
                        <?php echo $add1relationship; ?>

                        <h5> お名前 </h5>
                        <?php echo $add1nameSei; ?> <?php echo $add1nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $add1furiganaSei; ?> <?php echo $add1furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $add1gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $add1year; ?>年　<?php echo $add1month; ?>月 <?php echo $add1day; ?>日


                        <h3>追加入居者(2)</h3>
                        <h5> 続柄 </h5>
                        <?php echo $add2relationship; ?>

                        <h5> お名前 </h5>
                        <?php echo $add2nameSei; ?> <?php echo $add2nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $add2furiganaSei; ?> <?php echo $add2furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $add2gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $add2year; ?>年　<?php echo $add2month; ?>月 <?php echo $add2day; ?>日


                        <h3>追加入居者(3)</h3>
                        <h5> 続柄 </h5>
                        <?php echo $add3relationship; ?>

                        <h5> お名前 </h5>
                        <?php echo $add3nameSei; ?> <?php echo $add3nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $add3furiganaSei; ?> <?php echo $add3furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $add3gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $add3year; ?>年　<?php echo $add3month; ?>月 <?php echo $add3day; ?>日


                        <h3>追加入居者(4)</h3>
                        <h5> 続柄 </h5>
                        <?php echo $add4relationship; ?>

                        <h5> お名前 </h5>
                        <?php echo $add4nameSei; ?> <?php echo $add4nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $add4furiganaSei; ?> <?php echo $add4furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $add4gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $add4year; ?>年　<?php echo $add4month; ?>月 <?php echo $add4day; ?>日


                        <h3>追加入居者(5)</h3>
                        <h5> 続柄 </h5>
                        <?php echo $add5relationship; ?>

                        <h5> お名前 </h5>
                        <?php echo $add5nameSei; ?> <?php echo $add5nameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $add5furiganaSei; ?> <?php echo $add5furiganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $add5gender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $add5year; ?>年　<?php echo $add5month; ?>月 <?php echo $add5day; ?>日


                        <h3>緊急連絡先</h3>

                        <h5> 続柄 </h5>
                        <?php echo $ECRelation; ?>

                        <h5> お名前 </h5>
                        <?php echo $ECnameSei; ?> <?php echo $ECnameMei; ?>

                        <h5> フリガナ </h5>
                        <?php echo $ECfuriganaSei; ?> <?php echo $ECfuriganaMei; ?>

                        <h5> 性別 </h5>
                        <?php echo $ECgender; ?>
                        <h5> 生年月日 </h5>
                        <?php echo $ECyear; ?>年　<?php echo $ECmonth; ?>月 <?php echo $ECday; ?>日

                        <h5> 現住所 </h5>
                        <?php echo $ECpref; ?>
                        <?php echo $ECadreess1; ?>
                        <?php echo $ECadreess2; ?>
                        <?php echo $ECadreess3; ?>

                        <h5> 携帯電話番号 </h5>
                        <?php echo $ECmobilenumber; ?>

                        <h5> 携帯電話番号 </h5>
                        <?php echo $ECphonenumber; ?>

                        <h5> 本人確認書類(1) </h5>
                        <?php echo $_SESSION['file'] ?>

                        <h5> 本人確認書類(2) </h5>
                        <?php echo $_SESSION['file2'] ?>
                    </div>
                    <input class="backbtn" type="button" value="内容を修正する" onclick="history.back(-1)">
                    <button type="submit" name="submit" class="submit_btn">送信する</button>
                </form>
            </div>
        </section>

    </main>
    <footer>
        <div class="inner">
            <a href="index.html" class="logo">
                <img src="img/logoL.svg" alt="SkyYardProperty株式会社">
            </a>
            <div class="footer_contents">
                <div class="footer_linkGroup">
                    <p class="title">Address</p>
                    <p>〒105-0004<br>東京都港区新橋3-1-11<br>長友ランディックビル 6階</p>
                </div>
                <div class="footer_linkGroup">
                    <p class="title">Contacts</p>
                    <p><a href="contact.html">Mail:info@sky-yard.co.jp</a></p>
                    <a href="tel:0366722471">Tel:03-6672-2471</a>
                    <p>Fax:03-6672-7638</p>
                </div>
                <div class="footer_linkGroup link">
                    <p class="title">Links</p>
                    <div>
                        <a href="corporate.html">企業概要</a>
                        <a href="service.html">事業内容</a>
                        <a href="results.html">取り扱い実績</a>
                        <a href="recruit.html">採用</a>
                        <a href="contact.html">お問い合わせ</a>
                        <a href="privacy.html">利用規約</a>
                    </div>
                </div>
            </div>
            <div class="bnr_wrap">
                <a class="suumoBnr" href="https://suumo.jp/chintai/kaisha/kc_030_003511000/" target="_blank">
                    <img src="img/suumo_bnr.jpg" alt="SUUMOに物件掲載中。こちらからスーモで物件をチェックする">
                </a>
                <a class="requestBnr" href="contact.html">
                    <img src="img/request_bnr.jpg" alt="物件リクエスト受付中！SUUMOに掲載していない物件も対応可能。物件を指定してお探しの方はこちらから">
                </a>
            </div>
        </div>
        <small>&copy; 2022 SkyYardProperty Inc.</small>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</body>

</html>